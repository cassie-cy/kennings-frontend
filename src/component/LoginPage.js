import React from 'react';
import { Link, Redirect } from "react-router-dom";

import './LoginPage.css';

export default class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { username: '', password: '', loginstate: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.inputOnFocus = this.inputOnFocus.bind(this);
        this.inputOnBlur = this.inputOnBlur.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit(event) {
        this.getConnect();
        event.preventDefault();
    }

    inputOnBlur = () => {
        const regExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$/;
        if (!this.state.username) {
            this.setState({ isEmailFormatCorrect: 'This field is required.' });
        }
        else if (!regExp.exec(this.state.username)) {
            this.setState({ isEmailFormatCorrect: 'Please enter a valid username address.' });
        } else {
            this.setState({ isEmailFormatCorrect: '' });
        }
    }

    inputOnFocus = () => {
        if (this.state.isEmailFormatCorrect) {
            this.setState({ isEmailFormatCorrect: '' });
        }
    }

    getConnect() {
        let text = { username: this.state.username, password: this.state.password } 
        let send = JSON.stringify(text);  
        fetch('http://localhost:5000/api/user/login', {  
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send
        }).then(res => {
            if (res.status == 200) {
                res.json().then(res => {
                    alert(res.message);
                    localStorage.setItem('loginstate', 'true');
                    this.setState({ loginstate: 'true' });
                    
                    
                });
            } else {
                res.json().then(res => {
                    alert(res.errorMessage);
                });
            }
        });
    }

    render() {
        if (localStorage.loginstate) {
            return <Redirect to='/user/account' />
        } else {
            return (
                <div className='Container'>
                    <div className='Login'>
                        <div className='Login-title'>Login</div>
                        <form className='LoginForm' onSubmit={this.handleSubmit}>
                            <div className='LoginInput'>
                                <div className='title'>Username</div>
                                <input type='text' name='username' placeholder='E-mail address'
                                    value={this.state.username} onChange={this.handleChange}
                                    onBlur={this.inputOnBlur} onFocus={this.inputOnFocus}></input>
                                <div className='tips'>{this.state.isEmailFormatCorrect}</div>
                            </div>
                            <div className='LoginInput'>
                                <div className='title'> Password</div>
                                <input type='password' name='password' placeholder='Password'
                                    value={this.state.password} onChange={this.handleChange}></input>
                                <a herf='#'>Forget password?</a>
                            </div>
                            <input className='SubmitButton' type="submit" value="Log In"></input>
                        </form>
                    </div>
                    <div className='Register'>
                        <h1>New to Kennings?</h1>
                        <p>It is only a few minutes to create an acount and you can:</p>
                        <ul>
                            <li>Save shopping list and wish list, then access them from any devices.</li>
                            <li>View details of previous order.</li>
                            <li>Save you payment methods and enjoy quick buy.</li>
                        </ul>
                        <div className='Create'>
                            <Link to='/register'>
                                Create an Account
                            </Link>
                        </div>
                    </div>
                </div>
            );
        }
    }
}