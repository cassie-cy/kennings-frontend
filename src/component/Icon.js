import React from 'react';
import './Icon.css';

// control the size of icon
const Icon = (props) => {
    return (
        <div className='Icon'>
            <img src={props.src} width={`${props.width}`} height={`${props.height}`} />
        </div>
    );
}

export default Icon;