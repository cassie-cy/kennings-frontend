import React from 'react';
import Icon from './Icon';
import './HomeBody.css';

const Adv1 = () => (
    <div className='Adv1'>
        <div className='Adv1-item'>
            <Icon src={require('../static/flight.svg')} width='20px' height='20px' />
            <p>48 HR EXPRESS DELIVERY</p></div>
        <div className='Adv1-item'>
            <Icon src={require('../static/truck.svg')} width='20px' height='20px' />
            <p>SUPER FAST FREE DELIVERY</p></div>
        <div className='Adv1-item'>
            <Icon src={require('../static/card.svg')} width='20px' height='20px' />
            <p>INSTANT FREE CREDIT</p></div>
    </div>
)

const Adv2 = () => (
    <div id='Adv2'>
        <div className='Adv2-container'>
            <img src={require('../static/adv1.jpg')} />
            <img src={require('../static/adv2.jpg')} />
            <img src={require('../static/adv1.jpg')} />
            <img src={require('../static/adv2.jpg')} />
            <img src={require('../static/adv1.jpg')} />
        </div>
    </div>
);


const PopCategories = () => (
    <div className='PopCategories'>
        <div className='Title'>
            <h1>POPULAR CATEGORIES</h1>
        </div>
        <div className='Categories-image'>
            <div className='PopImage'>
                <img src={require('../static/pop1.jpg')} />
                <div className='desc'>
                    sheds
                </div>
            </div>
            <div className='PopImage'>
                <img src={require('../static/pop2.jpg')} />
                <div className='desc'>
                    Garden power tools
                </div>
            </div>
            <div className='PopImage'>
                <img src={require('../static/pop1.jpg')} />
                <div className='desc'>
                    add information
                </div>
            </div>
            <div className='PopImage'>
                <img src={require('../static/pop2.jpg')} />
                <div className='desc'>
                    add information
                </div>
            </div>
            <div className='PopImage'>
                <img src={require('../static/pop1.jpg')} />
                <div className='desc'>
                    add information
                </div>
            </div>
            <div className='PopImage'>
                <img src={require('../static/pop2.jpg')} />
                <div className='desc'>
                    add information
                </div>
            </div>
            <div className='PopImage'>
                <img src={require('../static/pop1.jpg')} />
                <div className='desc'>
                    add information
                </div>
            </div>
            <div className='PopImage'>
                <img src={require('../static/pop2.jpg')} />
                <div className='desc'>
                    add information
                </div>
            </div>
        </div>
        <div className='ViewMore'>
            <a className='ViewMore-link' href='#'>
                <span className='View-more-text'>View More</span>
            </a>
        </div>
    </div>
);

const Idea = () => (
    <div className='Idea'>
        <div className='Title'>
            <h1>IDEA & ADVICE</h1>
        </div>
        <div className='Idea-item'>
            <div className='Idea-image'>
                <img src={require('../static/idea1.jpg')} />
                <div className='Image-describe'>
                    <p>Outdoor living</p>
                    <ul>
                        <li>Garden furniture buying guide</li>
                        <li>Barbecue buying guide</li>
                        <li>Hot tub buying guide</li>
                        <li>Garden heating buying guide</li>
                    </ul>
                </div>
            </div>
            <div className='Idea-image'>
                <img src={require('../static/idea2.jpg')} />
                <div className='Image-describe'>
                    <p>Laminate & wood flooring</p>
                    <ul>
                        <li>Laminate & wood flooring buying guide</li>
                        <li>Laying laminate flooring</li>
                        <li>How to prepare to lay laminate & real wood flooring</li>
                        <li>How to repair floorboards</li>
                    </ul>
                </div>
            </div>
            <div className='Idea-image'>
                <img src={require('../static/idea3.jpg')} />
                <div className='Image-describe'>
                    <p>Kitchen design & planning</p>
                    <ul>
                        <li>4 steps to planning your kitchen</li>
                        <li>How to choose an installer for your project</li>
                        <li>Kitchen refresh ideas</li>
                        <li>Contemporary kitchen design ideas</li>
                    </ul>
                </div>
            </div>
        </div>

        <div className='ViewMore'>
            <a className='ViewMore-link' href='#'>
                <span className='View-more-text'>View More</span>
            </a>
        </div>
    </div>
);

const BackToTop = () => (
    <div className='Title'>
        {/* <h3>Back To Top</h3>
        <Icon src={require('../static/top.svg')} width='20px' height='20px' /> */}
    </div>
);

export default class HomeBody extends React.Component {

    render() {
        return (
            <div>
                <Adv1/>
                <Adv2 />
                <PopCategories />
                <Idea />
                <BackToTop />
            </div>
        );
    }
}