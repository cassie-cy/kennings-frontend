import React from 'react';
import './RegisterPage.css';
import { Link } from "react-router-dom";

export default class RegisterPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            forename: '',
            surname: '',
            mobile: '',
            username: '',
            password: '',
            confirmPassword: '',
            contactPreferences: '',
            isEmailFormatCorrect: '',

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.usernameOnBlur = this.usernameOnBlur.bind(this);
        this.usernameOnFocus = this.usernameOnFocus.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    usernameOnBlur = () => {
        const regExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$/;
        if (!this.state.username) {
            this.setState({ isEmailFormatCorrect: 'This field is required.' });
        }
        else if (!regExp.exec(this.state.username)) {
            this.setState({ isEmailFormatCorrect: 'Please enter a valid username address.' });
        } else {
            this.setState({ isEmailFormatCorrect: '' });
        }
    }

    usernameOnFocus = () => {
        if (this.state.isEmailFormatCorrect) {
            this.setState({ isEmailFormatCorrect: '' });
        }
    }

    handleSubmit(event) {
        this.getConnect();
        event.preventDefault();
    }

    getConnect() {
        let text = {
            forename: this.state.forename,
            surname: this.state.surname,
            mobile: this.state.mobile,
            username: this.state.username,
            password: this.state.password,
        }
        let send = JSON.stringify(text);
        fetch(`http://localhost:5000/api/user/register`, {
            method: `POST`,
            headers: { "Content-Type": "application/json;charset=utf-8" },
            body: send,
        }).then(res => {
            if (res.status == 201) {
                res.json().then(res => {
                    alert(res.message);
                    window.location.href = '/user/account'
                });
            } else {
                res.json().then(res => {
                    alert(res.errorMessage);
                    
                });
            }
        });
    }

    render() {
        return (
            <form className='RegisterPage' onSubmit={this.handleSubmit}>
                <div className='RegisterPage-title'>Create an Acount</div>
                <div className='RegisterPage-Container'>
                    <div className='RegisterPage-subtitle'>Your details</div>
                    <div className='Register-form'>
                        <div className='Register-name'>Forename</div>
                        <input type='text' style={{ textTransform: 'uppercase' }} name='forename'
                            value={this.state.forename} onChange={this.handleChange} />
                        <div className='Register-name'>Surname</div>
                        <input type='text' style={{ textTransform: 'uppercase' }} name='surname'
                            value={this.state.surname} onChange={this.handleChange} />
                        <div className='Register-name'>Mobile</div>
                        <input type="number" name='mobile' value={this.state.mobile} onChange={this.handleChange} />
                    </div>
                </div>
                <div className='RegisterPage-Container'>
                    <div className='RegisterPage-subtitle'>Username & Password</div>
                    <div className='Register-form'>
                        <div className='Register-name'>Email/Username</div>
                        <input type='text' name='username' value={this.state.username}
                            onChange={this.handleChange} onBlur={this.usernameOnBlur} ></input>
                        <div className='check-username'>{this.state.isEmailFormatCorrect}</div>
                        <div className='Register-name'>Password</div>
                        <input name='password' type='password' value={this.state.password}
                            onChange={this.handleChange}></input>
                        <div className='Password-tips'>
                            Passwords must be between 8-20 characters and include at
                            least three of the following; lowercase letters, uppercase
                    letters, numbers or special characters.</div>
                        <div className='Register-name'>Confirm you Password</div>
                        <input type='password'></input>
                    </div>
                </div>
                <div className='RegisterPage-Container'>
                    <div className='RegisterPage-subtitle'>Contact Preference</div>
                    <div className='Contact-preference'>I would like to receive news and marketing from Kennings via:</div>
                    <ul>
                        <li>
                            <input type="checkbox"></input>
                            Email
                        </li>
                        <li>
                            <input type="checkbox"></input>
                            SMS
                        </li>
                        <li><input type="checkbox"></input>
                            Post
                        </li>
                    </ul>
                    <div className='Privacy-policy'>
                        <input type="checkbox"></input>
                        I have read and understood the <div>Privacy policy</div>
                        {/* Create an acount */}
                    </div>
                    <input className='SubmitBtn' herf='/' type="submit" value="Create an acount" />
                    <div className='BacktoLogin' >
                        <Link to='/login'>Back to login</Link>
                    </div>
                </div>
            </form>
        )
    }
}