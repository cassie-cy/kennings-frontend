import React from 'react';
import Icon from './Icon';
import './Header.css';
import { Link } from "react-router-dom";

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loginstate: '', keyword: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleChange(event) {
        this.setState({ keyword: event.target.value });
    }

    handleSearch(event) {
        console.log(event);
    }

    getConnect() {
        fetch(`http://localhost:5000/api/products/${this.state.keyword}`, {
            method: 'GET',
            credentials: 'include',
        }).then(res => res.json())
            .then((data) => console.log(data))
    }

    //render header information
    renderHead1() {
        return (
            <div className='Head1'>
                <div className='SocialMedia'>
                    <a href='https://www.facebook.com/eDecksDecking' target='_blank'>
                        <Icon src={require('../static/social_fb.svg')} width='20px' height='20px' />
                    </a>
                    <a href='https://www.instagram.com/edecks.co.uk/' target='_blank'>
                        <Icon src={require('../static/instagram.svg')} width='20px' height='20px' />
                    </a>
                    <a href='https://www.pinterest.co.uk/edeckscouk/' target='_blank'>
                        <Icon src={require('../static/pinterest.svg')} width='20px' height='20px' />
                    </a>
                    <a href='https://twitter.com/edecksdecking' target='_blank'>
                        <Icon src={require('../static/social_twitter.svg')} width='20px' height='20px' />
                    </a>
                </div>
                <div className='Tel'>
                    <Icon src={require('../static/phonecall.svg')} width='20px' height='20px' />
                    <span>123456789</span>
                    <Icon src={require('../static/email.svg')} width='20px' height='20px' />
                    <span>example@outlook.com</span>
                </div>
            </div>
        );
    }

    //render searching bar
    renderHead2() {
        return (
            <div className='Head2'>
                <div className='Logo'>
                    <Link to='/'>
                        <div>Kennings</div>
                    </Link>
                </div>

                <form className='Search-field' action='/search' method='get'>
                    {/* <div className='Search-field'> */}
                        <Icon src={require('../static/search.svg')} width='30px' height='30px' />
                        <input type='text' name='keyword' placeholder="Search..." value={this.state.keyword} onChange={this.handleChange}/>
                    {/* </div> */}
                </form>

                <div className='Header2-item'>
                    <div className='UserCart'>
                        <Link to='/shoppingcart'>
                            <Icon src={require('../static/cart.svg')} width='40px' height='40px' />
                        </Link>
                        <Link to='/user/login'>
                            <Icon src={require('../static/user.svg')} width='40px' height='40px' />
                        </Link>
                    </div>
                </div>
            </div>
        );
    }

    //render navigation
    renderHead3() {
        return (
            <div className='Head3'>
                <div className='Navbar'>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                            <Link to='/products/materials'> Building Materials</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Cement & Aggregates</div>
                                    <a herf='#'>Aggregates</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Pre-mixed Cement&Mortar</a>
                                    <a herf='#'>Levelling Compounds</a>
                                    <a herf='#'>Lime</a>
                                </div>
                                <div className='column'>
                                    <div>Insulation</div>
                                    <a herf='#'>PIR Boards</a>
                                    <a herf='#'>Glass Wool</a>
                                    <a herf='#'>Rock Wool</a>
                                    <a herf='#'>Foils</a>
                                    <a herf='#'>Polystyrene</a>
                                </div>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                                <div className='column'>
                                    <div>Civils & Groundworks</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                                <div className='column'>
                                    <div>Plaster & Plasterbard</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                            <Link to='/products/timber'>Timber & Sheet Materials</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Timber</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                            <Link to='/products/landscaping'>Landscaping</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                        <Link to='/products/roofing'>Roofing & Insulation</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                        <Link to='/products/plumbing'>Plumbing & Electrical</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                        <Link to='/products/windows'>Windows, Doors & Joinery</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                        <Link to='/products/tools'>Tools & Tool Hire</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                        <Link to='/products/fixing'>Fixings and Ironmongery</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='dropdown'>
                        <button className='dropbtn'>
                        <Link to='/products/decorating'>Decorating</Link>
                        </button>
                        <div className='dropdown-content'>
                            {/* <div className='header'>
                        <h2>mega menu</h2>
                    </div> */}
                            <div className='row'>
                                <div className='column'>
                                    <div>Bricks & Blocks</div>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                    <a herf='#'>Facing bricks</a>
                                </div>
                                <div className='column'>
                                    <div>Aggregate and Cement</div>
                                    <a herf='#'>Aggregate</a>
                                    <a herf='#'>Sand</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                    <a herf='#'>Cement</a>
                                </div>
                                <div className='column'>
                                    <div>Drainage</div>
                                    <a herf='#'>Below Ground Drainage & Ducting</a>
                                    <a herf='#'>Above Ground Drainage</a>
                                    <a herf='#'>Access Covers & Gratings</a>
                                    <a herf='#'>Surface Water Solutions</a>
                                    <a herf='#'>Foundation Materials</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className='Head'>
                {this.renderHead1()}
                {this.renderHead2()}
                {this.renderHead3()}
            </div>
        );
    }
}


