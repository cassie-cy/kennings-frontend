import React from 'react';

import './OrderHistory.css';

export default class OrderHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
        };
        console.log(this.state.orders)
    }

    componentDidMount() {
        fetch('http://localhost:5000/api/user/orderhistory', {
            method: 'GET',
            credentials: 'include',
        })
        .then(res => res.json())
        .then((data) => {
            this.setState({ orders: data });
        })
        .catch(err => {
            alert(err);
        });
    }

    render() {
        if (Object.keys(this.state.orders).length == 0) {
            return (
                <div className='empty'>
                    <img src={require('../static/empty_shoppingBag.svg')} ></img>
                    <div >{this.state.empty}</div>
                    <div>Your order history is currently empty.</div>
                </div>
            )
        } else {
            const orders = this.state.orders.map((order) => {
                const products = order.products.map((product) => {
                    return (
                        <div className='product' key={product._id}>
                            <img src={`http://localhost:5000/${product.product.picture}`} />
                            <div className='productInfo'>
                                <div className='product-name'>{product.product.name}</div>
                                <div className='product-otherinfo'>
                                    <div className='product-price'>£{product.product.price}</div>
                                    <div className='btn'>
                                        <button>write a product review</button>
                                        <button>Hiden Order</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                });
                return (
                    <div className='orderInfo' key={order._id}>
                        <div className='orderDtails'>
                            <div className='date'>Date: {order.date}</div>
                            <div className='total'>Total price: £{order.totalPrice}</div>
                            <div className='id'>order id:{order._id}</div>
                        </div>
                        <div>{products}</div>
                    </div>
                );
            });
            return (
                <div>
                    <div className='account-title'>Order History</div>
                    <div>{orders}</div>
                </div>
            );
        }
    }
}