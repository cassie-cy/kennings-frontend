import React from 'react';
import './Bottom.css';

const BottonContent = () => (
    <div className='Botton'>
        <div className='BottonName'>
            <ul>
                <h4>Help & Support</h4>
                <li>Signing in/Registering</li>
                <li>Ordering</li>
                <li>Collection & Delivery</li>
                <li>Payment</li>
                <li>Return & Refund</li>
                <li>terms&Conditions</li>
            </ul>
        </div>

        <div className='BottonName'>
            <ul>
                <h4>Business</h4>
                <li>About us</li>
                <li>Career</li>
                <li>Business service</li>
            </ul>
        </div>

        <div className='BottonName'>
            <ul>
                <h4>Community</h4>
                <li>Facebook</li>
                <li>Twitter</li>
                <li>Pinterest</li>
                <li>Instagram</li>
            </ul>
        </div>     
    </div>
);

export default class Botton extends React.Component {
    render () {
        return (
            <BottonContent />
        );
    }
}