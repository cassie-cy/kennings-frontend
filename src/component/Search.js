import React from 'react';
import './ProductList.css';
import Icon from './Icon';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { object } from 'prop-types';

export default class Search extends React.Component {
    constructor(props) {
        super(props);
        const queries = new URLSearchParams(window.location.search);
        const keyword = queries.get('keyword');
        this.state = { products: [], product_id: '', keyword };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        this.getConnect();
        event.preventDefault();
    }

    getConnect() {
        let text = { product_id: this.state.product._id }
        let send = JSON.stringify(text);
        fetch('http://localhost:5000/api/user/shoppingcart', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send
        }).then(res => {
            if (res.status == 201) {
                res.json().then(res => {
                    alert(res.message);
                });
            } else {
                res.json().then(res => {
                    alert(res.errorMessage);
                });
            }
        })
    }

    componentDidMount() {
        // const category = params.get('category');
        fetch(`http://localhost:5000/api/search?keyword=${this.state.keyword}`, {
            method: `GET`,
            headers: { "Content-type": "application/json;charset=utf-8" },
        })
            .then(res => res.json())
            .then((data) => {
                this.setState({ products: data });
                console.log(typeof (this.state.products));
            }).catch(err => {
                alert(err);
            });
    }

    render() {
        const ListProducts = this.state.products.map((product) =>
            (<div className='ProductImage'>
                <Link to={`/product/${product._id}`}>
                    <img src={`http://localhost:5000/${product.picture}`} />
                    <div className='desc'>{product.name}
                        <div className='price'>£{product.price}</div>
                    </div>
                </Link>
                <button className='CartBtn' type="submit" onClick={this.handleClick}>
                    Add to shopping cart
                </button>
            </div>
            ));
        if (Object.keys(this.state.products) == 0) {
            return (
                <div className='empty_searching'>
                    <div className='empty_searching_content'>No result for your searching.</div>
                    <div className='empty_searching_tips'>Tip: Try rephrasing your search term, or using a related term.</div>
                </div>
            )
        } else {
            return (
                <div className='ProductListContent'>
                    <div className='ProductAdv'>
                        <img src={require('../static/productadv.jpg')} />
                    </div>
                    <div className='ProductListContentContainer'>
                        <div className='Filter'>
                            <div className='FilterTitle'>Filters:</div>
                            <a href='#'>clear all</a>
                            <div className='FilterName'>
                                Brands
                        <Icon src={require('../static/plus.svg')} width='15px' height='15px' />
                            </div>
                            <div className='FilterName'>
                                Style
                        <Icon src={require('../static/plus.svg')} width='15px' height='15px' />
                            </div>
                            <div className='FilterName'>
                                Price
                        <Icon src={require('../static/plus.svg')} width='15px' height='15px' />
                            </div>
                            <div className='FilterName'>
                                Star rating
                        <Icon src={require('../static/plus.svg')} width='15px' height='15px' />
                            </div>
                            <div className='FilterName'>
                                Size
                        <Icon src={require('../static/plus.svg')} width='15px' height='15px' />
                            </div>
                            <div className='FilterName'>
                                Color
                        <Icon src={require('../static/plus.svg')} width='15px' height='15px' />
                            </div>
                        </div>
                        <div className='Product-list'>
                            {ListProducts}
                        </div>
                    </div>
                </div>
            );
        }
    }
}