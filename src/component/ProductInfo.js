import React from 'react';
import './ProductInfo.css';
import Icon from './Icon.js';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class productInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { product: [], number: 1 };

        this.handleShoppingCartClick = this.handleShoppingCartClick.bind(this);
        this.handleSaveForLaterClick = this.handleSaveForLaterClick.bind(this);
        this.handleNumberChange = this.handleNumberChange.bind(this);
    }

    handleShoppingCartClick(event) {
        this.getConnectToShoppingCart();
        event.preventDefault();
    }

    handleSaveForLaterClick(event) {
        this.getConnectToSaveForLater();
        event.preventDefault();
    }

    handleNumberChange(event) {
        this.setState({ number: event.target.value });
    }

    getConnectToShoppingCart() {
        const send = JSON.stringify({
            product_id: this.state.product._id,
            number: this.state.number,
        });
        fetch('http://localhost:5000/api/user/shoppingcart?op=inc', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send
        }).then(res => {
            if (res.status == 200) {
                res.json().then(res => {
                    alert('Added successful.');
                });
            } else {
                res.json().then(res => {
                    alert(res.errorMessage);
                });
            }
        })
    }

    getConnectToSaveForLater() {
        const send = JSON.stringify({
            product_id: this.state.product._id,
            number: this.state.number,
        });
        console.log(send)
        fetch('http://localhost:5000/api/user/saveforlater?op=inc', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send
        }).then(res => {
            if (res.status == 200) {
                res.json().then(res => {
                    alert('Added successful.');
                });
            } else {
                res.json().then(res => {
                    alert(res.errorMessage);
                });
            }
        })
    }

    addToShoppingCart() {
        this.props.add({
            product_id: this.state.product_id,
            number: this.state.number
        });
    }

    componentDidMount() {
        const id = this.props.match.params.product_id;
        fetch(`http://localhost:5000/api/product/${id}`, {
            method: `GET`,
            headers: { "Content-type": "application/json;charset=utf-8" },
        }).then(res => res.json())
            .then((data) => {
                this.setState({ product: data.productInfo });
            });
    }

    render() {
        return (
            <div className='ProductInfo'>
                <div className='Product-Nav'>
                    <Icon src={require('../static/home.svg')} width='20px' height='20px' />
                    <div> Home </div>
                    <Icon src={require('../static/right.svg')} width='20px' height='20px' />
                    <div> Timber </div>
                    <Icon src={require('../static/right.svg')} width='20px' height='20px' />
                    <div> Treated Timber Decking Joint </div>
                </div>
                <div className='ProductSpecInfo'>
                    <div className='ProductImage'>
                        <img src={`http://localhost:5000${this.state.product.picture}`} />
                    </div>
                    <div className='ProductSpec'>
                        <div className='ProductName'>{this.state.product.name}</div>
                        <div className='ProductCode'>Product code: {this.state.product.code}</div>
                        <div className='Price'>£{this.state.product.price}</div>
                        <div className='Star'>
                            <img src={require('../static/star.svg')} width='15px' height='15px' />
                            <img src={require('../static/star.svg')} width='15px' height='15px' />
                            <img src={require('../static/star.svg')} width='15px' height='15px' />
                            <img src={require('../static/star.svg')} width='15px' height='15px' />
                            <img src={require('../static/star-empty.svg')} width='15px' height='15px' />
                            <a href='#'>review</a>
                        </div>
                        <div className='QuantityInput'>
                            <p>Quantity:</p>
                            <input type='Number' id='quatity' min='1' max='1000' placeholder='1'
                                value={this.state.number} onChange={this.handleNumberChange}></input>
                        </div>
                        <div className='Total'>
                            <p>Total price:</p>
                            <p className='TotalPrice'>£{this.state.product.price}</p>
                        </div>
                        <button className='ButtonContent' type="submit" onClick={this.handleShoppingCartClick}>
                            <Icon src={require('../static/trunk.svg')} width='20px' height='20px' />
                            Add to Shopping Cart
                        </button>
                        <button className='ButtonContent' type="submit" onClick={this.handleSaveForLaterClick}>
                            <Icon src={require('../static/trunk.svg')} width='20px' height='20px' />
                            Save For Later
                        </button>
                    </div>
                </div>
                <div className='ProductMore'>
                    <ul>
                        <li>
                            <Icon src={require('../static/plus.svg')} width='20px' height='20px' />
                            Product Details</li>
                        <li><Icon src={require('../static/plus.svg')} width='20px' height='20px' />
                            Technical Information</li>
                        <li><Icon src={require('../static/plus.svg')} width='20px' height='20px' />
                            Product Reviews</li>
                    </ul>
                </div>
                <div className='You-might-like'>
                    <h2>You Might Also Like</h2>
                    <div className='ProductImage'>
                        <img src={require('../static/timber1.jpg')} />
                        <div className='desc'>
                            Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m
                    <div className='price'>£3.41</div>
                        </div>
                        <div className='add'>
                            add to shopping cart
                    </div>
                    </div>
                    <div className='ProductImage'>
                        <img src={require('../static/timber2.jpg')} />
                        <div className='desc'>
                            Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m
                    <div className='price'>£3.41</div>
                        </div>
                        <div className='add'>
                            add to shopping cart
                    </div>
                    </div>
                    <div className='ProductImage'>
                        <img src={require('../static/timber3.jpg')} />
                        <div className='desc'>
                            Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m
                    <div className='price'>£3.41</div>
                        </div>
                        <div className='add'>
                            add to shopping cart
                    </div>
                    </div>
                    <div className='ProductImage'>
                        <img src={require('../static/timber4.jpg')} />
                        <div className='desc'>
                            Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m
                    <div className='price'>£3.41</div>
                        </div>
                        <div className='add'>
                            add to shopping cart
                    </div>
                    </div>
                </div>
                <div className='You-recent-view-products'>
                    <h2>You Recent View Products</h2>
                    <div className='You-recent-view-products-container'>
                        <div className='ProductImage'>
                            <img src={require('../static/timber6.jpg')} />
                            <div className='desc'>
                                Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m
                            <div className='price'>£3.41</div>
                            </div>
                            <div className='add'>
                                add to shopping cart
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}