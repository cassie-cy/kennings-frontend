import React from 'react';
import { withCookies } from 'react-cookie';
import { Route, Redirect, Link, Switch } from 'react-router-dom';

import AccountInfo from './AccountInfo';
import OrderHistory from './OrderHistory';
import SaveForLater from './SaveForLater';
import './Account.css';

class Account extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loginstate: localStorage.getItem('loginstate') ? true : false,
		}
		this.handleSignout = this.handleSignout.bind(this);
	}

	// handle sign out
	handleSignout() {
		console.log(this.state.loginstate)
		fetch('http://localhost:5000/api/user/signout', {
			method: 'DELETE',
			credentials: 'include',
		}).then(res => {
			if (res.status == 204) {
				localStorage.removeItem('loginstate');
				this.setState({ loginstate: false });
			} else {
				alert('Signout failed.');
			}
		});
	}

	// render login page
	render() {
		if (this.state.loginstate) {
			const { match } = this.props;
			console.log(match);
			return (
				<div className='account-container'>
					<div className='account-nav'>
						<div className='account-nav-title'>
							<div className='account-nav-title-left'>My Account</div>
							<div className='account-nav-title-right' onClick={this.handleSignout}>Signout</div>
						</div>
						<div className='nav-list'>
							<ul>
								<Link to='/user/account'>
									<li>Personal details</li>
								</Link>
								<li>Addresses</li>
								<li>Payment</li>
								<Link to='/user/account/orderhistory'>
									<li>Order history</li>
								</Link>
								<Link to='/user/account/saveforlater'>
									<li>Save for later list</li>
								</Link>
							</ul>
						</div>
					</div>
					<div className='accountContent'>
						<Switch>
							<Route exact path='/user/account' component={AccountInfo} />
							<Route path='/user/account/orderhistory' component={OrderHistory} />
							<Route path='/user/account/saveforlater' component={SaveForLater} />
						</Switch>
					</div>
				</div>
			)
		} else {
			return <Redirect to='/user/login' />
		}
	}
}

export default withCookies(Account);