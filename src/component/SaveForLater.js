import React from 'react';

import './SaveForLater.css';

export default class SaveForLater extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            saveForLater: {}
        };

        this.changeQuantity = this.changeQuantity.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.inputOnBlur = this.inputOnBlur.bind(this);
    }

    changeQuantity(e) {
        const cart = this.state.saveForLater;
        cart[e.target.name].number = e.target.value;
        this.setState({ saveForLater: cart });
    }

    handleClick(e) {
        this.getConnectToShoppingCart(e);
        e.preventDefault();
    }

    inputOnBlur(e) {
        const send = JSON.stringify({
            number: e.target.value,
            product_id: e.target.name,
        })
        fetch('http://localhost:5000/api/user/saveforlater?op=mod', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send,
        }).then(res => {
            if (res.status == 200) {
                res.json().then(alert('Changed successful.'))
            } else {
                res.json().then(res => {
                    alert(res.errorMessage)
                });
            }
        })
    }

    getConnectToShoppingCart(e) {
        const key = e.target.name;
        const send = JSON.stringify({
            product_id: e.target.name,
            number: e.target.value,
        });
        fetch('http://localhost:5000/api/user/saveforlater/move', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send,
        }).then(res => {
            if (res.status == 200) {
                alert('move to shopping cart successfully')
                const save = this.state.saveForLater;
                delete save[key];
                this.setState({ saveForLater: save });
            } else {
                alert(res.errorMessage)
            }
        })
    }

    componentDidMount() {
        fetch('http://localhost:5000/api/user/saveforlater', {
            method: 'GET',
            credentials: 'include',
        }).then(res => {
            if (res.status == 200) {
                res.json().then(res => {
                    const save = {}
                    res.map(ele => {
                        save[ele.product._id] = ele;
                        this.setState({ saveForLater: save })
                    })
                })
            } else {
                alert(res.message)
            }
        })
    }

    render() {
        const saveForLater = Object.keys(this.state.saveForLater).map((key) => {
            const ele = this.state.saveForLater[key];
            return (
                <div className='saveforlater-product' key={ele.product._id}>
                    <img src={`http://localhost:5000/${ele.product.picture}`} />
                    <div className='saveforlater-productInfo'>
                        <div className='saveforlater-product-name'>{ele.product.name}</div>
                        <div className='saveforlater-product-otherinfo'>
                            <div className='product-price'>£{ele.product.price}</div>
                            <div>quantity:
                            <input className='product-quantity' name={ele.product._id} value={ele.number}
                                    onBlur={this.inputOnBlur} onChange={this.changeQuantity}></input>
                            </div>
                        </div>
                        <div className='btn'>
                            <button onClick={this.handleClick} value={ele.number}
                                name={ele.product._id}>add to shopping cart</button>
                        </div>
                    </div>
                </div>
            )
        });

        if (Object.keys(this.state.saveForLater) == 0) {
            return (
                <div className='empty'>
                    <img src={require('../static/empty_list.svg')} ></img>
                    <div >{this.state.empty}</div>
                    <div>Your save for later list is currently empty.</div>
                </div>
            )
        } else {
            return (
                <div>
                    <div className='account-title'>Save For Later</div>
                    <div>{saveForLater}</div>
                </div>
            )
        }
    }
}