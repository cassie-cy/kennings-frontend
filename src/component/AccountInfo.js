import React from 'react';
import { Redirect } from 'react-router-dom';

import './AccountInfo.css';

export default class AccountInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            forename: '',
            surname: '',
            mobile: '',
            username: '',
            address: [],
            payment: [],
            saveForLater: [],
            loginstate: true,
        };

        this.renderAddress = this.renderAddress.bind(this)
        this.renderPayment = this.renderPayment.bind(this)
    }

    // fetch data of account information from the server
    componentDidMount() {
        if (localStorage.loginstate) {
            fetch('http://localhost:5000/api/user/account', {
                method: 'GET',
                credentials: 'include',
            }).then(res => {
                if (res.status === 200) {
                    return res.json();
                } else if (res.status === 401) {
                    throw { status: 401, errMessage: 'Not authorized.' }
                } else {
                    throw { status: res.status, errMessage: 'Interal Server Error.' }
                }
            }).then((data) => {
                this.setState({
                    forename: data.forename, surname: data.surname, mobile: data.mobile,
                    username: data.username, address: data.addresses, payment: data.paymentMethods,
                });
            }).catch(err => {
                if (err.status === 401) {
                    localStorage.removeItem('loginstate');
                    this.setState({ loginstate: false });
                } else {
                    alert(err);
                }
            });
        } else {
            this.setState({ loginstate: false });
        }
    }

    renderPayment() {
        if (this.state.payment.length === 0) {
            return (
                <div>Your payment method is empty.</div>
            )
        } else {
            return (
                this.state.payment.map((payment) =>
                    (<div className='payment' key={payment._id}>
                        <div>Ending in {payment.cardNumber.substr(payment.cardNumber.length - 4)}</div>
                        <div>Name on card: {payment.name}</div>
                        <div>Exp. {payment.expiryDate}</div>
                        <div>card type:{payment.cardType}</div>
                    </div>
                    ))
            )
        }
    }

    renderAddress() {
        if (this.state.address.length === 0) {
            return (
                <div> Your address is empty.</div>
            )
        } else {
            return this.state.address.map((address) =>
                (<div className='address' key={address._id}>
                    <div>{address.flatNumber}</div>
                    <div>{address.streetAddress}</div>
                    <div>{address.address2}</div>
                    <div>{address.city}</div>
                    <div>{address.country}</div>
                    <div>{address.postcode}</div>
                </div>));
        }
    }

    //check login state before rendering data, if not state then redirect to login.
    render() {
        if (this.state.loginstate) {
            return (
                <div className='account-info'>
                    <div className='info-title'>My Account</div>
                    <div className='personal-detail'>
                        <div className='info-subtitle'>
                            <div className='info-subtitle-left'>Personal details</div>
                            <div className='info-subtitle-right'>manage</div>
                        </div>
                        <div className='info-content'>
                            <div>forename: {this.state.forename}</div>
                            <div>surname: {this.state.surname}</div>
                            <div>mobile: {this.state.mobile}</div>
                            <div>username: {this.state.username}</div>
                        </div>
                    </div>
                    <div className='addresses'>
                        <div className='info-subtitle'>
                            <div className='info-subtitle-left'>Addresses</div>
                            <div className='info-subtitle-right'>manage</div>
                        </div>
                        <div className='info-content'>
                            {this.renderAddress()}
                        </div>
                    </div>
                    <div className='payment'>
                        <div className='info-subtitle'>
                            <div className='info-subtitle-left'>Payment</div>
                            <div className='info-subtitle-right'>manage</div>
                        </div>
                        <div className='info-content'>
                            {this.renderPayment()}
                        </div>
                    </div>
                </div>
            )
        } else {
            return <Redirect to='/user/login' />
        }
    }
}


