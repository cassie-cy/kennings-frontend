import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Link } from "react-router-dom";

import './ShoppingCart.css';
import Icon from './Icon';

export default class ShoppingCart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { shoppingCart: {} };
        this.changeQuantity = this.changeQuantity.bind(this);
        this.inputOnBlur = this.inputOnBlur.bind(this);
        this.handleCheckout = this.handleCheckout.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    componentDidMount() {
        if (localStorage.loginstate) {
            fetch('http://localhost:5000/api/user/shoppingcart', {
                method: 'GET',
                credentials: 'include',
            }).then(res => {
                if (res.status == 200) {
                    res.json().then(res => {
                        const cart = {}
                        res.shoppingCart.map(ele => {
                            cart[ele.product._id] = ele;
                        });
                        this.setState({ shoppingCart: cart });
                        // }
                    })
                } else {
                    res.json().then(data => {
                        alert(data.errorMessage)
                    })
                }
            })
        }
    }

    changeQuantity(e) {
        const cart = this.state.shoppingCart;
        cart[e.target.name].number = e.target.value;
        this.setState({ shoppingCart: cart });
    }

    inputOnBlur(e) {
        const send = JSON.stringify({
            number: e.target.value,
            product_id: e.target.name,
        })
        fetch('http://localhost:5000/api/user/shoppingcart?op=mod', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send,
        }).then(res => {
            if (res.status == 200) {
                res.json().then(alert('Changed successful.'))
            } else {
                res.json().then(res => {
                    alert(res.errorMessage)
                });
            }
        })
    }

    handleCheckout(e) {
        const send = JSON.stringify({
            products: Object.keys(this.state.shoppingCart).map(key => {
                const ele = this.state.shoppingCart[key];
                return {
                    product: ele.product._id,
                    number: ele.number,
                }
            }),
            totalPrice: e.target.value,
            status: 'submitted',
        });
        fetch('http://localhost:5000/api/user/orderhistory', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send,
        }).then(res => {
            if (res.status == 200) {
                res.json().then(res => {
                    alert('checkout successful.')
                    this.setState({ shoppingCart: {} });
                })
            } else {
                res.json().then(res => {
                    alert(res.errorMessage)
                });
            }
        })
    }

    handleRemove(e) {
        const key = e.target.name
        const send = JSON.stringify({
            product_id: key,
        })
        fetch('http://localhost:5000/api/user/shoppingcart', {
            method: 'DELETE',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send,
        }).then(res => {
            if (res.status == 204) {
                alert('remove successful.')
                const cart = this.state.shoppingCart;
                delete cart[key];
                this.setState({ shoppingCart: cart });
            } else {
                res.json().then(res => {
                    alert(res.errorMessage)
                })
            }
        })
    }

    handleSave(e) {
        const key = e.target.name
        const send = JSON.stringify({
            product_id: e.target.name,
            number: e.target.value,
        });
        fetch('http://localhost:5000/api/user/shoppingcart/move', {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: send,
        }).then(res => {
            if (res.status == 200) {
                alert('move to save for later successfully')
                const cart = this.state.shoppingCart;
                delete cart[key];
                this.setState({ shoppingCart: cart });
            } else {
                alert(res.errorMessage)
            }
        })
    }

    cart() {
        const shoppingCart = Object.keys(this.state.shoppingCart).map(key => {
            const ele = this.state.shoppingCart[key];
            return <div className='Collection-item'>
                <img src={`http://localhost:5000/${ele.product.picture}`} />
                <div className='Item-right'>
                    <div className='Item'>
                        <div className='Item-info'>
                            <div className='Item-description'>{ele.product.name}</div>
                            <div className='Item-price'>{`£${(ele.product.price).toFixed(2)}`}</div>
                        </div>
                        <div className='Item-change'>
                            <div className='Number'>
                                <div>quatity: </div>
                                <input type='Number' name={key} value={ele.number}
                                    onBlur={this.inputOnBlur} onChange={this.changeQuantity}></input>
                            </div>
                            {/* <div className='Change'>
                                <button className='Remove' name={key} onClick={this.handleRemove}>Remove</button>
                                <div className='Save-for-later'>Save for later</div>
                            </div> */}
                        </div>
                    </div>
                    <div className='change'>
                        <button className='change-btn' name={key} onClick={this.handleRemove}>Remove</button>
                        <button className='change-btn' name={key} value={ele.number} onClick={this.handleSave}>Save for later</button>
                    </div>
                </div>
            </div>
        })

        let subTotal = 0;

        Object.keys(this.state.shoppingCart).forEach(key => {
            const ele = this.state.shoppingCart[key];
            subTotal += parseFloat((ele.product.price * ele.number).toFixed(2));
        });
        let shippingCost = 10.00;

        return (
            <div className='Cart'>
                <div className='CartContainer-left'>
                    <div className='CartContainer1'>
                        <div className='Cart-title'>Shopping Cart</div>
                        <Link to='user/account/saveforlater'>
                            <div className='View-save-for-later'>View saved Lists</div>
                        </Link>
                    </div>
                    <div className='CartContainer2'>
                        <div className='Collection'>
                            <div className='Collection-title'>Add for Collection</div>
                            {shoppingCart}
                        </div>
                    </div>
                </div>
                <div className='CartContainer-right'>
                    <div className='Cart-title'>Order Summary</div>
                    <div className='Subtotal'>
                        <div>Subtotal</div>
                        <div className='Subtotal-price'>{`£${subTotal.toFixed(2)}`}</div>
                    </div>
                    <div className='Subtotal'>
                        <div>Delivery</div>
                        <div className='Subtotal-price'>{`£${shippingCost.toFixed(2)}`}</div>
                    </div>
                    <div className='Cart-total-price'>
                        <div>Total</div>
                        <div className='Subtotal-price'>{`£${(shippingCost + subTotal).toFixed(2)}`}</div>
                    </div>
                    <div className='Voucher'>
                        <Icon src={require('../static/plus.svg')} width='20px' height='20px' />
                        Add a discount code or voucher
                    </div>
                    <button className='Checkout-btn' value={(subTotal + shippingCost).toFixed(2)} onClick={this.handleCheckout} >
                        Proceed to Checkout
                    </button>
                </div>
            </div>
        );
    }

    boughtTogether = () => {
        return (
            <div className='BoughtTogether'>
                <div className='Cart-title'>
                    frequently bought together
                </div>
                <div className='BoughtTogether-product'>
                    <div className='ProductImage'>
                        <img src={require('../static/timber1.jpg')} />
                        <div className='desc'>
                            Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m
                            <div className='price'>£3.41</div>
                        </div>
                        <div className='add'>
                            add to shopping cart
                        </div>
                    </div>
                    <div className='ProductImage'>
                        <img src={require('../static/timber2.jpg')} />
                        <div className='desc'>
                            Sawn Treated Timber Softwood carcassing 19mm x 38mm x 3.6m
                            <div className='price'>£3.41</div>
                        </div>
                        <div className='add'>
                            add to shopping cart
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        if (Object.keys(this.state.shoppingCart) == 0) {
            return (
                <div className='empty'>
                    <img src={require('../static/empty_shoppingBag.svg')} ></img>
                    <div >{this.state.empty}</div>
                    <div>Your shopping cart is currently empty.</div>
                </div>
            )
        } else {
            return (
                <div className='shopping-cart'>
                    {this.cart()}
                    {this.boughtTogether()}
                </div>
            );
        }
    }
}
