import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { CookiesProvider } from 'react-cookie';

import './App.css';
import Header from './component/Header';
import Bottom from './component/Bottom';
import ProductList from './component/ProductList';
import ProductInfo from './component/ProductInfo';
import LoginPage from './component/LoginPage';
import RegisterPage from './component/RegisterPage';
import HomeBody from './component/HomeBody';
import ShoppingCart from './component/ShoppingCart';
import Account from './component/Account';
import Search from './component/Search';

class App extends React.Component {
  render() {
    return (
      <div className='App'>
        <CookiesProvider>
          <Router>
            <Header />
            <Switch>
              <Route exact path='/' component={HomeBody}></Route>
              <Route path='/products/:category' component={ProductList}></Route>
              <Route path='/product/:product_id' component={ProductInfo}></Route>
              <Route path='/user/login' component={LoginPage}></Route>
              <Route path='/register' component={RegisterPage}></Route>
              <Route path='/shoppingcart' component={ShoppingCart}></Route>
              <Route path='/user/account' component={Account}>
              </Route>
              <Route path='/user/saveforlater' component={Account}></Route>
              <Route path='/user/orderhistory' component={Account}></Route>
              <Route path='/search' component={Search}></Route>
              
            </Switch>
            <Bottom />
          </Router>
        </CookiesProvider>
      </div>
    );
  }
}

export default App;
